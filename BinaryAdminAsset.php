<?php
/*
 * 2014-10-28
 * @author Prawee Wongsa <konkeanweb@gmail.com>
 * http://binarycart.com/
 */

namespace prawee\theme\binaryadmin;

use yii\web\AssetBundle;

class BinaryAdminAsset extends AssetBundle
{
    public $sourcePath = '@vendor/prawee/yii2-theme-binary-admin/src';
    public $baseUrl = '@web';
    public $css = [
        'assets/css/font-awesome.css',
        'assets/js/morris/morris-0.4.3.min.css',
        'assets/css/custom.css',
    ];
    public $js = [
        'assets/js/jquery-1.10.2.js',
        'assets/js/jquery.metisMenu.js',
        'assets/js/morris/raphael-2.1.0.min.js',
        'assets/js/morris/morris.js',
        'assets/js/custom.js',
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public function init() {
        parent::init();
    }
}