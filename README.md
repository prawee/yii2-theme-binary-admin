Yii2 Binary Admin theme
=======================
Binary Admin theme  for Yii2 applicaiton

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist prawee/yii2-theme-binary-admin "*"
```

or add

```
"prawee/yii2-theme-binary-admin": "*"
```

to the require section of your `composer.json` file.


Usage
-----
